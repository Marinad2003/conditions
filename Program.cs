﻿using System;

namespace Conditions
{
    class Program
    {
       
        static void Main(string[] args)
        {
            Task_6();
        }


        static void Task_1()
        {
            Random rnd = new Random();

            int x = rnd.Next(1,5);
            int y = rnd.Next(1,5);
            if (x % 2 == 1)
            {
                Console.WriteLine($"Первое число нечетное");
            }
            else {
                Console.WriteLine($"Первое число четное");
            }
            if (y % 2 == 1)
            {
                Console.WriteLine($"Первое число нечетное");
            }
            else {
                Console.WriteLine($"Первое число четное");
            }
        }
        static void Task_2()
        {
            int x = Convert.ToInt32(Console.ReadLine());
            int y = Convert.ToInt32(Console.ReadLine());
            int z = Convert.ToInt32(Console.ReadLine());

            if (x>=y && x>=z)
            {
                Console.WriteLine($"{x} наибольшее число");
            }
            if (y >= x && y >= z)
            {
                Console.WriteLine($"{y} наибольшее число");
            }
            if (z >= y && z >= x)
            {
                Console.WriteLine($"{z} наибольшее число");
            }
            
        }
        static void Task_3()
        { 
            int r = Convert.ToInt32(Console.ReadLine());
            int dot_x = Convert.ToInt32(Console.ReadLine());
            int dot_y = Convert.ToInt32(Console.ReadLine());

            if (Math.Pow(dot_x,2)+Math.Pow(dot_y,2) == Math.Pow(r,2) )
            {
                Console.WriteLine("Точка на круге");
                
            }
            else if (dot_x > r && dot_y >= r)
            {
                Console.WriteLine("Точка не в круге");
            }

            if (dot_x >= r && dot_y >=r)
            {
                Console.WriteLine("Точка не в круге");
            }

            
        }
        static void Task_4()
        {
            Random number = new Random();

            int x = number.Next(0, 31);

            switch (x)
            { case int n when(n<10):
                    Console.WriteLine($"Число {x} диапазоне до 10");
                    break;

                case int n when (n >= 10  && n < 20):
                    Console.WriteLine($"Число {x} диапазоне от 10 до 20");
                    break;

                case int n when (n >= 20 && n <= 30):
                    Console.WriteLine($"Число {x} диапазоне от 20 до 30");
                    break;

            }

        }
        static void Task_5()
        {
            int year = Convert.ToInt32(Console.ReadLine());

            if (year % 4 != 0)
            {
                Console.WriteLine("Обычный год, ничего такого");
            }
            else if (year % 100 !=0)
            {
                Console.WriteLine("Високосный год!");
            }else if (year % 400 == 0)
            {
                Console.WriteLine("Високосный год!");
            }
            else
            {
                Console.WriteLine("Обычный год, ничего такого");
            }
           
        }
        static void Task_6()
        {
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());
            bool exist = false;
            bool isosceles = false;
            if (a == b || b ==c || c == a)
            {
                isosceles = true;
            }
            if (a+b >=c || a+ c >= b || b+ c >=a)
            {
                exist = true;
            }
            if (exist)
            {
                Console.Write("Triangle exists ");
                if (isosceles)
                {
                    Console.WriteLine("and it is isosceles");

                }

            }

        }
    }
}
